import React, {useContext} from 'react';
import {AppBar, Button, Grid, Toolbar} from "@material-ui/core";

import {Context} from '../index';
import {useAuthState} from 'react-firebase-hooks/auth';

const Navbar = () => {
    const {auth, firebase} = useContext(Context);
    const [user, loading, error] = useAuthState(auth);

    const login = async () => {
        const provider = new firebase.auth.GoogleAuthProvider();
        const {user} = await auth.signInWithPopup(provider);
    }

    return (
        <AppBar color={'primary'} position="static">
            <Toolbar variant={"dense"}>
                <Grid container justify={"flex-end"}>
                    {user
                        ? <Button onClick={() => auth.signOut()} variant={'outlined'}>Logout</Button>
                        : <Button variant={'outlined'} onClick={login}>Login</Button>
                    }
                </Grid>
            </Toolbar>
        </AppBar>
    )
};

export default Navbar;
