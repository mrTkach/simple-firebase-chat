import React, {useContext} from 'react';
import {Box, Button, Container, Grid} from "@material-ui/core";
import {Context} from '../index';
import {useAuthState} from 'react-firebase-hooks/auth';
import firebase from 'firebase';

const Login = () => {

    const {auth} = useContext(Context);
    const [user, loading, error] = useAuthState(auth);

    const login = async () => {
        const provider = new firebase.auth.GoogleAuthProvider();
        await auth.signInWithPopup(provider);
    }

    return (
        <Container>
            <Grid
                container
                alignItems={"center"}
                justify={"center"}
                style={{height: window.innerHeight - 50}}
            >
                <Grid
                    container
                    alignItems={'center'}
                    direction={'column'}
                    style={{width: 400, backgroundColor: 'lightgray'}}
                >
                    <Box p={5}>
                        <Button variant={"outlined"} onClick={login}>
                            Login with Google
                        </Button>
                    </Box>
                </Grid>
            </Grid>
        </Container>
    );
};

export default Login;