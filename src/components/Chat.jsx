import React, {useContext, useState} from 'react';
import {Context} from '../index';
import {useAuthState} from "react-firebase-hooks/auth";
import {useCollectionData} from "react-firebase-hooks/firestore";
import Loader from "./Loader";
import {Avatar, Button, Container, Grid, TextField} from "@material-ui/core";


const Chat = () => {

    const {auth, firestore, firebase} = useContext(Context);
    const [user] = useAuthState(auth);
    const [messages, loading, error] = useCollectionData(firestore.collection('messages').orderBy('createdAt'));

    const [value, setValue] = useState('');

    if (loading) {
        return <Loader />
    }

    const sendMessage = async () => {
        firestore.collection('messages').add({
            id: Date.now().toString(),
            uid: user.uid,
            displayName: user.displayName,
            photoURL: user.photoURL,
            text: value,
            createdAt: firebase.firestore.FieldValue.serverTimestamp()
        });
        setValue('');
    }

    return (
        <Container>
            <Grid
                container
                justify={"center"}
                style={{ height: window.innerHeight - 50, marginTop: '15px' }}
            >
                <div style={{width: '80%', height: "70vh", border: '1px solid gray', overflowY: 'auto'}}>
                    {messages && messages.map(message => (
                        <div
                            style={{
                                margin: 10,
                                border: user.uid === message.uid ? '2px solid green' : '1px solid blue',
                                marginLeft: user.uid === message.uid ? 'auto' : '10px',
                                width: 'fit-content',
                                borderRadius: 5,
                                padding: '5px'
                            }}
                        >
                            <Grid>
                                <Avatar src={message.photoURL} />
                                <div>{message.displayName}</div>
                            </Grid>
                            <div>{message.text}</div>
                        </div>
                    ))}
                </div>
                <Grid
                    container
                    direction={"column"}
                    alignItems={"flex-end"}
                    style={{width: "80%"}}
                >
                    <TextField
                        fullWidth
                        rowsMax={2}
                        variant={"outlined"}
                        value={value}
                        onChange={e=>setValue(e.target.value)}
                    />
                    <Button onClick={sendMessage} variant={"outlined"} style={{marginTop: '5px'}}>Send</Button>
                </Grid>
            </Grid>
        </Container>
    );
};

export default Chat;
