import React, {createContext} from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import firebase from 'firebase';
import 'firebase/auth';
import 'firebase/firestore';


// initialize Firebase
firebase.initializeApp(
    {
        apiKey: "AIzaSyDIyQgH7BHBOy4ooPQ-6U7rOT0IqRqfPZw",
        authDomain: "my-firebase-project-40f9c.firebaseapp.com",
        projectId: "my-firebase-project-40f9c",
        storageBucket: "my-firebase-project-40f9c.appspot.com",
        messagingSenderId: "1083162572632",
        appId: "1:1083162572632:web:e852d94e3cb74d42ea972d"
    }
)

export const Context = createContext(null);
const auth = firebase.auth();
const firestore = firebase.firestore();

ReactDOM.render(
    <Context.Provider value={{ firebase, auth, firestore }} >
        <App />
    </Context.Provider>
    , document.getElementById('root')
);
